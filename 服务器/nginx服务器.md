## Nginx是什么

- **一个高性能的`web代理服务器`**

- **nginx占用内存少、支持高并发，最高可达 5w 个并发量**

- **开源，可用于商业用途，使用配置简单，中文社区也比较活跃**

- **支持热部署 ，在不打断用户请求的情况下更新版本(:warning:是nginx版本，不是应用版本)**

## 正向和反向代理区别

![区别](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/正反代理.6y2of3fqgts0.jpg)

## nginx配置文件理解

> nginx默认的配置，也可以自行修改，换端口，换location的路径和index首页
  
  ![默认配置](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/默认配置.3l3m5kj9x720.png)

> 再来一张菜鸟上面的截图，很详细
 
   ![config](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/config.3xzdplv40rq0.jpg)

## nginx实现负载均衡的几种策略

**负载均衡其实就是利用多个服务器集群，将客户端的高并发请求分发到不同的服务器来减轻压力**

!> **简单的集群配置**
  
> 轮询(AB...AB...交替处理请求，相当于列表循环)
    
 ![简单集群](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/集群1.386v5fvlw2w0.jpg)
  
> 权重(weight)：
    
 ![权重](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/weight.761b1wuvit00.jpg)
  
> IP绑定(ip_hash)
   
 ![IP绑定](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/ip_hash.4x58fei6mxe0.jpg)
  
> 服务响应时间最短(fair):
    
 ![fair](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/fair.2eh1xuxxi3fo.jpg)
  
> 当前连接最少(least_conn)
    
 ![least_conn](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/nginx/least_conn.5pjvq3tpbr00.jpg)

## session共享问题

!> **当同一个用户第一次访问tomcat_8111 并且登录成功， 而第二次访问却被分配到了tomcat_8222， 这里并没有记录他的登陆状态，那么就会呈现未登录状态了，严重伤害了用户体验。**

> 解决办法：`使用ip_hash绑定策略`

## 高可用

### 反向代理服务器高可用

1. **当某一台服务器出现问题了，变成了down状态，我们要选择第二台服务器，所以我们在`location节点`下添加`proxy_connect_timeout 1`，目的是在连接超过1秒后(防止请求握手服务器失败后处于一直等待状态)，请求自动轮询到下一台没有宕机的服务器去。**

### nginx高可用

1. **使用keepalived来设置主从nginx，防止nginx宕机事故的发生，nginx宕机后启动第二个nginx**  [教程参考](https://www.cnblogs.com/920913cheng/p/10484786.html)

## nginx解决跨域

- **解决办法：在相对应的`location节点`中添加以下内容**

```yml
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

    if ($request_method = 'OPTIONS') {
        return 204;
    }
```

## nginx开启https协议

- **需要ssh证书，[教程参考]**(https://www.cnblogs.com/wl416896401/p/13903496.html)

## nginx静态资源部署(动静分离)

- **将静态资源部署在 `Nginx` 上(即：html页面放在nginx安装目录下的html文件夹中，图片等静态资源要在配置文件中的`server节点中的location节点中配置`)**

- **动态资源请求(一般为需要查接口查数据库的资源)则利用nginx做反向代理去对应的服务器后台获取数据**

- **使用前后端动静分离后，可提升静态资源的访问速度（因为不要查后台），即使动态服务不可用，静态资源的访问也不会受到影响。**

## 补充

!> **linux系统中，http请求默认端口是80，而nginx配置文件中默认的端口也是80，所以没有改端口的情况下，访问linux的ip，默认也是80端口。**
