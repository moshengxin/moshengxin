# Linux常用命令和操作总结

## Linux是什么

- **Linux 是在 1991 由林纳斯·托瓦兹在赫尔辛基大学上学时创立的，主要受到 Minix 和 Unix 思想的启发，所以Linux继承了Unix。**

- **Linux是开源的，目前我所了解到的Linux的发行版有Ubuntu和CentOS。当然还有其它的。本篇主要是在Ubuntu 20.04.1版本上操作Linux。**

- **目前国内 Linux 更多的是应用于服务器上，而桌面操作系统更多使用的是 Windows。**

## 相关环境准备

- **Ubuntu的运行要依赖于Vm虚拟机，关于Vm和Ubuntu的安装，可以点击这里[快点我](https://blog.csdn.net/hao5119266/article/details/89198275)，跟着教程一步步来。相信我，你可以的。**

## Linux常用命令总结

    //进入文件夹
    cd dir          
    //跳转到首次进入终端时的路径，相当于用户根目录吧。
    cd ~		
    //返回上一级，如果是../..则代表返回上两级
    cd ..		
    //返回进入此目录之前所在目录(注意和cd..区分开来)
    cd - 		
    //查看当前目录下有什么内容
    ls	
    //罗列出当前文件或目录的详细信息，含有时间、读写权限、大小、时间等信息 	
    ll	
    //查看当前所在的全路径	
    pwd 
    //清除屏幕所有命令		
    clear	
    //把file文件移动到dir目录，其中file也可以是一个目录	
    mv file dir	
    //把file文件复制一份到dir目录
    cp -r fileurl dir        
    //解压xxx.tar.gz到当前目录     
    tar zxvf xxx.tar.gz     
    //把xxx.tar.gz解压到指定的dir文件夹      
    tar zxvf xxx.tar.gz -C dir   
    //参数为文件或者文件夹的路径。使用sudo会要求用户输入密码。   
    sudo rm -r 参数      
    //查看ubuntu版本号         
    cat /etc/issue	             
    //安装软件 
    sudo apt install [software]  
    //卸载软件
    sudo apt remove [software]   
    //创建test.txt文件
    touch test.txt

- **如果不想每次安装都用`sudo`，则要使用`su root`切换到root用户。ubuntu默认不开启root用户，如果要开启就要设置一个root密码，具体百度一下，这里不作介绍。**

## Linux终端里的颜色区分

> 蓝色------>目录

> 绿色------>可执行文件

> 红色------>压缩文件

> 浅蓝色--->链接文件

> 灰色------>其他文件

## vim编辑器日常使用

> vim编辑器有3种模式，分别是**命令模式、插入模式、底线命令模式**。
  
  ![s](https://s3.ax1x.com/2021/01/08/sn2Iqs.png)

> 在进行编辑时，你要注意的是模式的切换，vi编辑**默认进入命令模式(此时不能输入内容)**，如果想在文件中插入内容，则要先按下"i"，然后才可以输入，输入完后，再按Esc退出插入模式。然后按“shift+;”输入:冒号，进入底线命令。再输入wq保存退出。

> 如果在编辑过程中==出现键盘方向键变成ABCD==的情况，解决方法如下：


       //按顺序执行以下命令
       sudo apt-get remove vim-common
       sudo apt-get install aptitude
       sudo aptitude install vim

## vim设置永久显示行号命令

       //按顺序执行以下命令
       vim ~/.vimrc    //打开配置文件
       :set nu         //在编辑区中输入:set nu
       :wq             //保存并退出

!> **如果用:wq保存文件报“无法打开并写入文件”错误，请用下面的命令保存：**


       //忽略权限保存
       : w ! sudo tee %

## 一招教你区分`mv`命令

|                     命令                     |                                                                结果                                                                |
| :----------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------: |
|      mv source_file(文件) dest_file(文件)      |                                                将源文件名 source_file 改为目标文件名 dest_file                                               |
|    mv source_file(文件) dest_directory(目录)   |                                             将文件 source_file 移动到目标目录 dest_directory 中                                             |
| mv source_directory(目录) dest_directory(目录) | 目录名 dest_directory 已存在，将 source_directory 移动到目录名 dest_directory 中；目录名 dest_directory 不存在则 source_directory 改名为目录名 dest_directory |
|    mv source_directory(目录) dest_file(文件)   |                                                                出错                                                                |

## 防火墙相关命令

- **查看防火墙状态(running已开启、dead已关闭)**

  > systemctl status firewalld

- **关闭防火墙**

  > service firewalld stop

- **开启防火墙(重启用restart)**

  > service firewalld start

- **开放某个端口可以所有外网访问，例如开放8080端口**

  > firewall-cmd --zone=public --add-port=8080/tcp --permanent

- **移除防火墙的某个端口(例如：8080)**

  > firewall-cmd --zone=public --remove-port=8080/tcp

- **防火墙重新加载配置**

  > firewall-cmd --reload

- **查看开放的端口列表**
 
   > firewall-cmd --list-ports

## Linux软件安装(Ubuntu下)

- **Linux 大小写敏感**

- **如果发现使用"sudo apt-get install xxxx"命令报“在等待缓存锁：无法获得锁......由进程7358持有”错误的话，可以使用以下命令解决**


        //依次运行
        sudo rm /var/lib/dpkg/lock-frontend
        sudo rm /var/cache/apt/archives/lock
        sudo rm /var/lib/dpkg/lock

### tomcat安装

      //创建tomcat命令
      mkdir tomcat   
      //进入tomcat目录
      cd tomcat     
      //下载tomcat8.5压缩包
      wget https://mirrors.bfsu.edu.cn/apache/tomcat/tomcat-8/v8.5.61/bin/apache-tomcat-8.5.61.tar.gz  
      //解压到当前目录
      tar xzvf apache-tomcat-8.5.61.tar.gz   
      //然后进入 apache-tomcat-8.5.61
      cd apache-tomcat-8.5.61/   
      //进入到bin目录       
      cd bin     
      //启动tomcat，并自行在浏览器输入localhost:8080查看是否启动成功                       
      ./startup.sh      
      //停止tomcat                 
      ./shutdown.sh            

### mysql安装

        //以下是根据回忆写的。其中有可能会少了某一步，有待验证
    	//1、创建文件夹：
    	mkdir mysql
    	//2、cd mysql后下载rpm文件：
    	wget https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
    	//3、安装服务
    	yum install -y mysql-community-server
    	//4、启动mysql：
    	service mysqld start
    	//5、登录mysql:  
    	sudo mysql -u root -p
    	//6、查看所有库：
    	show databases;（ps:sql命令不要忘记结束;标记）
    	//7、选择使用某个库：
    	use 库名
    	//8、查看库中所有表：
    	show tables;

### mysql安装成功后，设置外网访问

- **1.防火墙开放mysql的3306端口**
 
   > firewall-cmd --zone=public --add-port=3306/tcp

- **2.防火墙重新加载配置**
  
  > firewall-cmd reload

- **3.进入/etc/mysql/mysql.conf.d中的mysqld.cnf文件**
  
  > cd /etc/mysql/mysql.conf.d

- **4.用vi编辑命令进入文件并注释掉bind-address  = 127.0.0.1，保存并退出**
  
  > vi mysqld.cnf 

- **5.重启mysql后，外网便可连接成功**
  
  > systemctl resatrt mysql或者service mysql restart

### nginx安装

        //1、安装：
        sudo apt install nginx
        //2、查看版本：
        nginx -V
        //3、启动nginx：
        sudo /etc/init.d/nginx start
        //4、浏览器访问：localhost:80显示欢迎页代表启动成功(nginx默认端口80)

### top命令查看Linux进程相关

> 查看所有进程：top

> 退出top命令行：q

> 按Cpu占比排序(高--低)：shift + p

> 按内存占比排序(高--低)：shift + m

> 切换排序列：shift + >或者shift + \<

> top命令默认的排序列是“%CPU”。

> 高亮显示排序列：x

> 查看8080端口占用的PID进程：lsof -i:8080（主要看项目yml配置文件用的是哪个端口）  

> 杀进程：kill -9 PID代号   
