# redis 还是要会的

## redis 是什么

!> **redis 是一个高性能的非关系型数据库，相对于普通的关系型数据库，redis 的数据储存在缓存中，读(110000 次/s)写(81000 次/s )数据非常快，Redis 也支持数据的持久化，可以将内存中的数据保存在磁盘中，重启的时候可以再次加载进行使用。**

## redis 数据类型了解一下

> String: 字符串

> Hash: 散列

> List: 列表

> Set: 集合

> Sorted Set: 有序集合

## redis 数据类型常见应用场景

1. **String：缓存字符串(如 token，验证码，json 字符串，等)， 计数器、Session 共享、分布式 ID、分布式锁等。**

2. **Hash：可以用来存储一些 key-value 对，更适合用来存储对象**

3. **List 表：做栈或者队列，可以用来缓存类似微信公众号、微博等消息流数据**

4. **Set：不可重复性，Set 可以进行交集、并集、差集操作，从而可以实现类似，我和某人共同关注的人、朋友圈点赞等功能**

5. **Sorted Set 有序集合：排行榜功能**

## redis 的安装和简单使用(windows 环境下)

### 安装

> 1. 下载地址：<https://github.com/tporadowski/redis/releases。>

> 2. 下载 Redis-x64-xxx.zip 压缩包并解压到 D:\\redis

### windows 启动（两种方式）

- **启动方法有 cmd 命令启动和 exe 文件启动。其实他们都一样，命令只是暗地里操作 exe 文件启动而已。**

- **启动成功标志，出现 6379**

#### 方式一

- **因没有配置 redis 的环境变量，所以使用 cmd 命令启动时要注意切换到 redis 的安装目录(解压目录)D:\\redis,**

- **输入 redis-server.exe redis.windows.conf 回车，会生成一个窗口，注意不要关闭这个服务端窗口。**

![yqOlvV.png](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/test0423/redis-server.exe.3etu5brf2sa0.jpg)

- **然后再重新打开一个 cmd 窗口，输入 redis-cli.exe -h 127.0.0.1 -p 6379，用于打开 redis 客户端，可查看 redis 相关信息和命令练习**

![yqOsbD.png](https://s3.ax1x.com/2021/02/23/yqOsbD.png)

#### 方式二

- **exe 文件启动，按照下图顺序启动服务端和客户端**

![yqLSS0.png](https://s3.ax1x.com/2021/02/23/yqLSS0.png)

### linux 启动

- **cd 到 redis 安装的`bin`目录中，启动`redis-server`服务，启动完成了就运行`./redis-cli`打开客户端，然后就可以增删改了。**

- **如果要远程连接 linux 下的 redis，首先要确保 redis-server 服务要启动成功，并且 6379 端口要开放。然后在 cmd 中通过命令`redis-cli -h 10.106.252.131 -p 6379`连接，如果提示`(error) NOAUTH Authentication required.`错误，则使用命令`auth redis密码`来授权即可。**

## redis 常用命令学习总结

> redis 共有 16 个库，顺序从下标 0 开始。默认是第一个库，如要切换库，则要使用命令 select index 例如：select 1 代表切换到第二个库。

> 查看库中所有的键： keys *

> 命令太多要清屏： clear

### 老老实实做一个 redis 练习生

!> <a>会唱、跳、rap、篮球还不够，还要会 redis 基础命令，下面是每个类型相关。

#### String 相关

![yqv0vq.png](https://s3.ax1x.com/2021/02/23/yqv0vq.png)

![yqvrrV.png](https://s3.ax1x.com/2021/02/23/yqvrrV.png)

#### Hash 相关

![yqvhx1.png](https://s3.ax1x.com/2021/02/23/yqvhx1.png)

#### List 相关

![yqvbIe.png](https://s3.ax1x.com/2021/02/23/yqvbIe.png)

#### Set 相关

![yqvXRA.png](https://s3.ax1x.com/2021/02/23/yqvXRA.png)

#### Sorted Set 相关

偷一下懒，移步去看[菜鸟教程对应版块](https://www.runoob.com/redis/redis-sorted-sets.html)

## redis 的几个特性理解

!> 1. **缓存穿透**
   - **是指当 Redis 缓存中的 key 不存在 value 值时，就会请求到后台去查询数据库，如果并发量很高，就会造成数据库宕机**
   
   - **解决办法：法 1：查到空数据也进行缓存，但要求设置很短的过期时间，一般为几分钟。法二：布隆过滤器**

!> 2. **缓存击穿**
   - **是指 Redis 缓存中的 key 存在 value 值，但是过期了，该请求还是会去后台请求查询数据库，如果并发量很高同时查询同一条数据，也会造成数据库宕机。**
   
   - **解决办法：法一：直接设为永不过期就。法二：加互斥锁**

!> 3. **缓存雪崩**
   - **是指多条或者无数条数据过期了，造成系统崩溃（例如：热点商品秒杀）**
   
   - **解决办法：**
     
     - **法一：直接设为永不过期就。**
     
     - **法二：随机过期(过期一部分，目的防止同一时间大量数据过期现象发生。)**

## Redis 持久化的 rdb 方式和 aof 方式备份的区别

> RDB 每隔一段时间进行备份，AOF 每时每刻在写操作时进行备份，所以 AOF 备份出来的文件比 RDB 的要大，数据完整性也比 RDB 的要好。不过官方建议同时开启这两种持久化

> Redis 默认的持久化方式是 RDB ，并且默认是打开的

> 在配置文件中满足这些条件就会触发 RDB 备份操作

    save 900 1
    save 300 10
    save 60 10000

> 如果要开启 AOF 备份，则要在配置文件中把 appendonly 改为 yes 开启

> AOF 备份有三种方式，在配置文件中选择其中一种即可

    # appendfsync always       //实时，每个操作都会备份(IO开销大)
    appendfsync everysec      //每秒，redis宕机会丢失一秒的数据(建议使用)
    # appendfsync no

## Redis 数据恢复

- **如果有 AOF 文件,优先恢复 AOF，没有则恢复 RDB 文件**

## Redis 集群

### 集群定义

!> **所谓的集群，就是通过添加多个相同的服务器，防止某台服务器发生事故，导致系统不能正常运行，从而让服务器达到一个稳定、高效的状态。**
  
**主从复制**
    
> 主库可读、可写。从库只读不写，所以可以把查询统统交给从库，增删改就交给主库。

!> **ps：Master 主库有且仅有一个，建议从库最少设置两个**
 
**哨兵模式**
> 因为主从复制集群有一个致命的缺点，那就是主库发生故障，就不能进行增删改了，所以就产生了哨兵模式，

> 所谓的哨兵模式，就是当主库发生故障后，哨兵会在所有的从库中，自动选举一个新的库作为主库来进行增删改操作。

### 集群搭建

- [手把手教程](https://blog.csdn.net/qq_42815754/article/details/82912130)

### Redis 事务

- **相关命令：**

```java
multi               //开启事务
set .......        //数据库操作入队
exec/discard      //执行或取消队列
```

- **PS:Redis 不支持事务回滚，队列中的某一个操作失败，其它的剩余操作照样执行，因为 Redis 认为这种错误应该发生在开发环境中，不应该出现在正式环境中，Redis 并不帮程序员解决他造成的错误**

## linux 中(ubuntu)设置 redis 可远程访问

- :one: **找到 redis 的配置文件 redis.conf,该文件位于/etc/redis 文件夹下**
  > cd /etc/redis

- :two: **vi 编辑 redis.conf,把`bind 127.0.0.1 ::1`注释掉,并关闭保护模式，即把`protected-mode no`改为`yes`**

- :three: **防火墙开放 redis 的 6379 端口**
  > firewall-cmd --zone=public --add-port=6379/tcp --permanent

- :four: **重启防火墙配置**
  > firewall-cmd --reload

- :five: **重启 redis 服务就可以远程访问了**
