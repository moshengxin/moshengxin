# cmd命令备份本地postgres数据库以及还原

!> **1、postgres数据库的备份和还原，前提是都要进来pgadmin安装目录的bin文件夹**

!> **2、备份后的文件格式是.bak(其它格式我没有测试过)**

!> **3、本命令备份后，也只能用命令的方式还原，其它还原方式测试没有通过。**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200708160233915.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNjkyMDIx,size_16,color_FFFFFF,t_70)

## 备份

> 备份命令：pg_dump -h localhost -p 5432 -U postgres -d crcslocalhost> D:\\testbackup.bak 

> 口令为你的数据库密码，回车。备份成功。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200708160708497.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNjkyMDIx,size_16,color_FFFFFF,t_70)

## 还原

> 还原命令：psql -h localhost -p 5432 -U postgres -d test02 -f "D:\\testbackup.bak"

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200708161459480.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNjkyMDIx,size_16,color_FFFFFF,t_70)

