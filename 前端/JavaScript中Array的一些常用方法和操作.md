

<!--more-->
# JavaScript中Array的一些常用方法和操作

JavaScript的 Array 对象是用于构造数组的全局对象，数组是类似于列表的高阶对象

## 示例

### 数组创建

```javascript
//数组创建
var array = ['Apple', 'Banana'];
```

### 数组长度

```javascript
//数组长度
var length=array.length();   //1
```
### 数组取值

```javascript
//1、通过数组下标取值
var first = array[0];   //Apple
//2、for循环也可以取到想要的值
for(var index in array){
	console.log("下标"+index)
	console.log("值"+array[index])
}
```
### 添加数据

```javascript
//向数组中添加数据(可在for循环中连续添加数据)
array.push(obj)  //obj为插入对象或者数据
```
### 判断数据在数组中对下标

```javascript
//某个数据在数组中对下标
array.indexOf('Apple');  //0
```
### 替换数组对应下标的数据

```javascript
//替换数组对应下标的元素，index为你要替换的下标位置，replaceData为替换数据。
array.splice(index,1,replaceData)
```
### 数组连接或者合并

```javascript
//合并两个或多个数组(可为空数组)。此方法不会更改现有数组，而是返回一个新数组。
const array1 = ['a', 'b', 'c'];   //可为空数组
const array2 = ['d', 'e', 'f'];		//也可为空数组
const array3 = array1.concat(array2);  //["a", "b", "c", "d", "e", "f"]
```
### 查找满足条件的第一个值

```javascript
//返回数组中满足条件的第一个元素的值。否则返回 undefined。
const array1 = [5, 12, 8, 130, 44];
const found = array1.find(element => element > 10);  //从左到右，大于10的第一个数是12。
console.log(found);  //所以输出12
```
### 判断数组是否包含某个值

```javascript
// 判断数组是否包含某个值
const pets = ['cat', 'dog', 'bat'];
console.log(pets.includes('cat')); //true
```
### 数组转字符串，元素拼接成字符串

```javascript
// 数组转字符串，元素拼接成字符串
const elements = ['Fire', 'Air', 'Water'];
console.log(elements.join());  //"Fire,Air,Water"
console.log(elements.join('')); //"FireAirWater"
console.log(elements.join('-')); //"Fire-Air-Water"
```
### 数组倒置与排序

```javascript
//数组倒置
const array1 = ['1', '2', '3'];
console.log(array1.reverse()) //["3", "2", "1"]
//数组排序方法1
var numbers = [4, 2, 5, 1, 3];
numbers.sort(function(a, b) {
  return a - b;
});
console.log(numbers); //[1, 2, 3, 4, 5]
//数组排序方法2
var numbers = [4, 2, 5, 1, 3]; 
numbers.sort((a, b) => a - b); 
console.log(numbers);  //[1, 2, 3, 4, 5]
```
### 清空数组

```javascript
//方法1
array1= [];
//方法2 
array2.splice(0,array2.length);  
//方法3
array3.length = 0;
```

