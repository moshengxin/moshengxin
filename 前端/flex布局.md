# 没有flex之前

> 1. 调样式可以把前端搞得头大

> 2. 多端样式统一问题

> `虽然，现在前端组件模块开发也开始流行了，组件拿来即用，不用过多考虑样式问题，例如element，不过这里还是要总结一下flex相关`

# flex是什么

> flex也叫flex弹性布局，常用于设置弹性盒模型对象的子元素如何分配空间问题

> 把盒子变成弹性盒子`display: flex`

# flex基础

> flex分为`容器`和`容器中的子元素`两部分，子元素也叫项。

## 容器的属性

> 没加`display:flex`前，div都是往下叠加的，加了`display:flex`后，div变成往右叠加（即`flex-direction：row`）

```css
/**flex-direction：项目水平轴上（主轴，x轴）的排列方向*/
flex-direction: row;  /**从左往右一直叠加*/
flex-direction: row-reverse; /**从右往左一直叠加*/
flex-direction: column; /**从上往下一直叠加*/
flex-direction: column-reverse;  /**从下往上一直叠加*/
```

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/111.41e2exwwbt80.jpg)

> 容器内是否换行

```css
/** flex-wrap：决定项目超出范围是否换行 */ 
  flex-warp: nowarp: /**不换行*/
  flex-warp: warp: /**换行，第一行在上方*/
  flex-warp: warp-reverse: /**换行，第一行在下方*/
```

![换行](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/换行.572r91brrxg0.jpg)

> 对齐方式

```css
/** justify-content：项目在水平轴上（主轴）的对齐方式。 */
   align-items：/**项目在垂直轴上（交叉轴）如何对齐*/
   align-content：/**项目在两根轴线的对齐方式。如果项目只有一根轴线，该属性不起作用。*/
```

![居中相关属性](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/水平垂直居中.6ptt4zs9two0.jpg)

```css
/**  justify-content主轴和align-items交叉轴都适用，只是方向不一样。下图只举例主轴方向 */
flex-start：/**左对齐*/
flex-end：/**右对齐*/
center：/**居中*/
space-around：/**左右两边留出空间，目测是项目之间距离的一半*/
space-between：/**左右两边不留空间，项目之间平分空间*/
space-evenly：/**左右两边，项目之间的距离大小都一样*/
```

![space相关属性](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/对齐方式.5obu5j8phxc.jpg)

## 项目属性

> **order排序**：值越小越靠前，默认0

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/order属性.6hsmcc1vbyo0.jpg)

> **flex-grow放大**：默认0

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/flex-grow放大.26sylbvh6za8.jpg)

> **flex-shrink缩小**：和放大差不多，不过多介绍，默认1

> **flex-basis**：分配主轴上多余空间，默认auto

> **align-self**：会覆盖align-items属性

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/align-self.iy9txpx94f4.jpg)

> **flex**：`flex-grow`, `flex-shrink` 和 `flex-basis`的简写，默认值为`0 1 auto`（即不放大不缩小且自动分配空间）。后两个属性可省略，。

# flex实战

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/111.f2nfvjgcu34.jpg)

## 圣杯布局

> 经典的上中下布局，然后又在中间部分再进行一次分割，分割为三部分。

> 很多大型的网站采用的是圣杯布局，例如京东

> `发现一个快捷方式，按比例切分div空间，比如要1：5：1切分空间，则在第一个div设置flex：1，第二个div设置flex：5，第三个div设置flex:1即可`。

> flex:5代表1.  flex-grow: 5  flex-shrink: 1 flex-basis: 0% 这三个样式组合，flex:1代表 flex-grow: 1 flex-shrink: 1 flex-basis: 0%

```html
<div class="content">
    <div class="head">头部</div>
    <div class="body">
        <div class="body-left">body左边</div>
        <div class="body-center">body中间</div>
        <div class="body-right">body右边</div>
    </div>
    <div class="foot">脚部</div>
</div>
```

```css
.content {
    min-height: 100vh;
    border: 1px solid #333333;
    /*开启flex布局*/
    display: flex;
    flex-direction: column;
    text-align: center;
}

.head {
    border: 1px solid #333333;
    background-color: #4CD964;
    flex: 1;
}

.body {
    border: 1px solid #333333;
    background-color: #F0AD4E;
    flex: 5;
    /*body再进行一次flex布局*/
    display: flex;
}

.body-left {
    background-color: #f0ede1;
    flex: 1;
}

.body-center {
    background-color: #F0AD4E;
    /*
      flex-grow: 5;   放大5倍
      flex-shrink: 1;  不缩小
      flex-basis: 0%;  分配多余空间之前，项目占据的主轴空间
    */
    flex: 5;
}

.body-right {
    background-color: #f0ede1;
    flex: 1;
}

.foot {
    border: 1px solid #333333;
    background-color: #96a1e7;
    flex: 1;
}
```

![效果图](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/圣杯.1xai40g2p0cg.jpg)

## 头部和底部固定，中间自适应

> flex:数值，代表项目平分flex容器的剩余的空间，如果只有一个项目使用了flex属性，则它占用剩余的所有空间。

  ![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/foot固定.379hhyiokr20.jpg)

## 右下角返回顶部按钮

**实现思路**

> `在foot栏中加一个弹性div，控制flex容器中的子项目的主轴和交叉轴的对齐方式都右对齐就可以了`

![返回顶部](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/backTop.1wu2592sf3sw.jpg)

## 侧边导航栏布局（类似element）

**实现思路**

> 先左右划分成两半，左边15%，剩余空间给右边，再把右边分成上中下

```html
<div class="content">
    <div class="sidebar">侧边栏</div>
    <div class="body">
        <div class="body-head">上</div>
        <div class="body-center">中(主体)</div>
        <div class="body-foot">下</div>
    </div>
</div>
```

```css
.content {
    min-height: 100vh;
    display: flex;
    background-color: #aeb9b8;
    flex-direction: row;
}

.sidebar {
    background-color: #dbb4e7;
    flex: 0 1 15%;
}

.body {
    background-color: #3F536E;
    flex: 4;
    display: flex;
    flex-direction: column;
}

.body-head {
    background-color: #E7BFB4;
    flex: 0 1 10%;
}

.body-center {
    background-color: #808080;
    flex: 1;
}

.body-foot {
    background-color: #E7BFB4;
    flex: 0 1 10%;
}
```

![效果图](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/flex布局/element.5tl7plcuyys0.jpg)
