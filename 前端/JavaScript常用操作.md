

# String 对象

## indexOf 和 includes

- indexOf 返回`-1`代表不包含目标字符串，includes 返回`false`代表不包含目标字符串

- 两者都是判断 String 中是否包含目标字符串，但是有一些注意点要我们注意一下(`"",'',数值类型,null,undefined,NaN`)，详细看下图

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/11.2dv70o044mxw.jpg)
**拓展**：写到这里不禁想到表单的非空校验（包含空格的情况），可用 indexOf(“ ”)或者 includes(“ ”)来实现

## 字符串连接，判断，截取，分隔

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/22.13e6y7kn1jcw.jpg)

## substr 和 substring 区别

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/33.4pxs4ulkukg0.jpg)

## 去除首尾空格

- 使用`trimStart()`和`trimEnd()`

```javascript
" jack".trimStart()
// "jack"
" rose ".trimStart().trimEnd()
// "rose"
"  rose  ".trimStart().trimEnd()
// "rose"
"  ro se  ".trimStart().trimEnd()
// "ro se"
```

# Array 对象常用操作

## js 删除 list 元素，可躲过索引

```javascript
/*
@params: arr为要删除的数组，item为要删除的对象（选中的）
@return: arr1为新的数组，即没被删除的对象。
注意：原数组没有变化，
*/
remove(arr, item) {
    var arr1 = [];
    for (var i = 0; i < arr.length; i++) {

        if (arr[i] != item) {
            arr1.push(arr[i]);
        }
    }
    return arr1;
},
```

![](<https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/1625737634(1).fizv0tgare0.png>)

## 根据数组中对象的 key 值来删除元素

```javascript
// 删除name为空的数据
removeArrayByObjKey(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].name== '') {
            arr.splice(i, 1);
            break;
        }
    }
},
```

## JavaScript 操作 dom 刷新

- 在实际开发中，我们有时候会遇到 dom 中绑定的数据有变化，但是 dom 没有刷新变化，我们肉眼看不到效果，这时候，我们就可以使用下面的内容来强制刷新。`showTable为表格的v-if的值`

```javascript
// 强制刷新渲染dom
this.showTable = false;
this.$nextTick(() => {
    this.showTable = true;
});
```

## 判断 array 中所有的对象是不是相同

```javascript
var arrayExistSameObj = function(array) {
    if (array.length > 0) {
        return !array.some(function(value, index) {
            return value !== array[0];
        });
    } else {
        return true;
    }
}
arrayExistSameObj([0, 0])
// true
arrayExistSameObj([0, 0, 1])
// false
```

## 判断 array 中是否存在相同的元素

- 利用 Set 不可重复特性来判断，如果传入的 array 和 set 的大小一样。则没有相同元素。

```javascript
var objIsRepeat = function(array) {
    let s = new Set();
    array.forEach(obj => {

    /*如果数组中是对象，则用obj.key来判断*/
        s.add(obj)
    })
    return s.size == array.length ? false : true
}

```

![](<https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/1626016336(1).76tv0pf1u5g0.jpg>)

- 补充：

```javascript
objIsRepeat([1,2,3,4])
false
objIsRepeat([1,2,3,4,4])
true
// 可用上图解决。
objIsRepeat([{"name":"1"},{"name":"1"}])
false
// null undefined NaN等类型也适用
objIsRepeat([null,undefined,NaN])
false
objIsRepeat([null,null,undefined])
true
```

## concat 和 splice 和 slice 的区别

直接看图说话

- `concat连接两个数组，不改变原数组，生成新的数组`
- `splice删除和替换数组元素，也可添加元素，splice会改变原数组`
  ![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/22.3q88h6kx7so0.jpg)

- `slice截取数组,不会改变原数组`
  ![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/44.5ub0f8jxppg0.jpg)

## concat 和 push

- concat 连接不会改变原数组，push 向数组后面添加元素，会改变原数组。
  ![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/55.78g1xtxrzys0.jpg)

## join 数组分隔

```javascript
var list = [-1, 0, 1, 2, 3, 4, 5, 6, 7]
// 默认用逗号连接分隔 输出"-1,0,1,2,3,4,5,6,7"
list.join()
// 输出"-1-0-1-2-3-4-5-6-7"
list.join("-")
// 输出"-1A0A1A2A3A4A5A6A7"
list.join("A")

```

## 数组排序

- 一般的数值类型的数组，直接 array.sort()就可以排序了，但是对象数组要根据对象中的某个字段来排序就要自己实现
  ![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/123.27jckmq942f4.jpg)

# Date 对象

- 一张图，记一下就好了
  ![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/444.10n8itb7pwio.jpg)

# JSON 对象

- 我日常工作中常用的也就两个方法，第一个是`JSON.parse()，把字符串转JSON对象`，第二个是`JSON.stringify()，把JSON对象转字符串`

# Math 对象

## Math.random()生成随机数

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/77.38dydec10oo0.jpg)

## Math.round()四舍五入

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/66.2nwr675m6e00.jpg)

## Math.abs()求绝对值

![](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/微信消息推送/55.1c024u0qrszk.jpg)

