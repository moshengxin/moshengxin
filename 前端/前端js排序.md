

<!--more-->

## 前端在js中排序数据
>和后端排序类似，this.sortdata为你要降序处理的数据

```
this.sortdata.sort(function (a,b) {
                return a.value-b.value;
            })
```

>如果要升序：

```
 this.sortdata.sort(function (a,b) {
                return b.value-a.value;
            })
```

!> **注：a.value中的value为sortdate中的值。即你要根据sortdata中的value值来排序。如果sortdata中你要根据其它值来排序(如：年龄、分数、数量等等)，则自行替换**
