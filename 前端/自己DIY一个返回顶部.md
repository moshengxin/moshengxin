# 自己 DIY一个返回顶部
## 方法

- **锚点定位法（不好实现平滑滚动）**
> 通过a标签定位到ID为top的元素   `<a href="#top" target="_self">Top</a>`

- **scroll相关方法（可方便实现平滑滚动）**

    - [jquery](https://jquery.cuishifeng.cn/scroll.html)

    - [javascript](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/scroll)

## 准备知识

- `document.documentElement.scrollTop` **是获取滚动条滚动的距离**
- `display` **控制Dom元素的显示和隐藏，`none` 为隐藏**
- `onscroll 事件` **在元素滚动条在滚动时触发**


## 开始
1. **先在自己网页底部做一个按钮位置固定，我这里直接一个a标签包一个按钮，也可直接按钮，看你喜欢哪个。**

> 如果想要平滑滚动，则使用`scroll({top: 0,left: 0,behavior: 'smooth'})`

```html
<a id="scroll-to-top" href="javascript:scroll(0,0)" target="_self" class="scroll-to-top">
  <button>Top</button>
</a>
```

2. **Css样式，`display`我设置为默认不显示**

```css
.scroll-to-top {
    position: fixed;
    top: 90%;
    right: 0;
    display: none;
}
```

3. **脚本控制，滚动条滚动到一定距离显示返回顶部按钮，这里默认100**

```javascript
window.onscroll = function() {
    if (document.documentElement.scrollTop < 100) {
        //滚动条小于100不显示
        document.getElementById("scroll-to-top").style.display = "none"
    } else {
        document.getElementById("scroll-to-top").style.display = "inline"
    }
}
```
