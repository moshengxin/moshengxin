
## 交接文档

## 商品录入插件相关
> 代码具体位置

![image.png](https://pic.rmb.bdstatic.com/bjh/cc337e3bef03df3797f791db6e4ecc1a.png)

> 组件相关
![image.png](https://pic.rmb.bdstatic.com/bjh/ddf438b70b1c36f8c30eac4a53154f62.png)

## 飞书消息推送相关

> 代码具体位置

![image.png](https://pic.rmb.bdstatic.com/bjh/665120c4fba9eb5336e4ddfd0cc84252.png)


- 如果要更换飞书机器人，要创建一个新的并替换配置即可，对应 `sys_params`表，param_code为`FEISHU_OPEN_API_CONFIG_KEY`
- [飞书机器人官方文档](https://open.feishu-pre.cn/document/uAjLw4CM/ukTMukTMukTM/bot-v3/bot-overview)

## 邮件相关
> 代码位置
![image.png](https://pic.rmb.bdstatic.com/bjh/9a7d2ffd77c080d7eb673b1849b50f3e.png)

### 邮件相关配置
- 对应 `sys_params`表，param_code 为 `ALI_MAIL_CONFIG_KEY`
