# Docker是什么

> Docker 将应用程序与该程序的依赖(可理解为运行环境和软件支持，如mysql、redis、nginx、es、mq等等)，打包在一个文件里面。运行这个文件，就会生成一个虚拟容器。程序在这个虚拟容器里运行，就好像在真实的物理机上运行一样。有了 Docker，就不用担心环境问题。

> 总体来说，Docker 的接口相当简单，用户可以方便地创建和使用容器，把自己的应用放入容器。容器还可以进行版本管理、复制、分享、修改，就像管理普通的代码一样。

# Docker有什么用

> Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

> 容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

# Docker优点

- `启动快`、`资源占用小`、`体积小`

# Docker应用场景(什么时候用)

- Web 应用的自动化打包和发布
  - 如果要发布应用到几十台甚至几百台服务器时，利用docker可大大提高效率。
- 自动化测试和持续集成、发布、快速交付软件给用户。
  - 合作开发的时候，在本机可以跑，别人的电脑跑不起来，如果利用docker把应用依赖的所有环境(java、tomcat、redis等)都打成一个镜像，直接丢给别人的电脑上就能运行起来，很方便。
- 在服务型环境中部署和调整数据库或其他的后台应用。

# Docker怎么用

## Docker三个概念

### 镜像

- 镜像不包含任何动态数据，其内容在构建之后也不会改变(可理解为web应用运行时需要的环境)
- 镜像有多个tag，也就是说多个版本，比如java有多个版本，redis也有多个版本这样，我们docker pull image时可以指定tag版本(docker pull java:8)，不指定则默认最新版本latest。

### 容器

- 容器是用镜像创建的运行实例，一个镜像可以创建多个容器。
- 容器之间相互隔离，保证应用运行期间的安全。

### 仓库

- 仓库（Repository）是集中存放镜像文件的场所，类似github。

## 数据卷(volume)

- 卷就是目录或文件，存在于一个或多个容器中，由docker挂载到容器，但不属于联合文件系统
- 卷的设计目的就是数据的持久化，完全独立于容器的生存周期，因此Docker不会在容器删除时删除其挂载的数据卷
- `docker run -it -v /宿主机目录:/容器内目录`命令中的`-v`后面跟着的就是数据卷
  - 特点：
    1：数据卷可在容器之间共享或重用数据
    2：卷中的更改可以直接生效
    3：数据卷中的更改不会包含在镜像的更新中
    4：数据卷的生命周期一直持续到没有容器使用它为止

## Docker目录简析

- /var/lib/docker/image/overlay2/repositories.json里边存的是镜像，对应docker images命令结果
- /var/lib/docker/image/overlay2/imagedb/content/sha256里边存放的是历史镜像id文件(包括已删除的)，vi 镜像id 可查看相关信息
- 

## 安装

<a><b> [ubuntu安装docker教程](https://www.jianshu.com/p/80e3fd18a17e)

## 常用命令

### Docker启动相关命令

<a><b> docker启动

> systemctl start docker    # 方法一
> docker         # 方法二
> docker start $(docker ps -a | awk '{ print $1}' | tail -n +2) #启动所有容器

<a><b> 查看docker是否正在运行【Active: <font color=green>active (running)</font>运行状态 】
【Active: inactive (dead) 停止状态】

> systemctl status docker 

<a><b> 关闭docker

> systemctl stop docker

<a><b> 重启docker

> systemctl restart docker

<a><b> 查看Docker版本

> docker version   或者  docker info

### 镜像相关命令

<a><b> 搜索仓库中的java镜像

> docker search java

<a><b> 从仓库中拉取java镜像到本地

> docker pull java     # 不指定java版本号
> docker pull java:8   # 指定java8版本

<a><b> 查看所有镜像

> docker images

<a><b> 删除镜像

> docker rmi 镜像NAME

<a><b> 强制删除所有镜像

> docker rmi -f $(docker images)

<a><b> 镜像打包成tar包

> docker image save 镜像NAME/ID > 镜像.tar

<a><b> tar包还原成镜像(导入镜像)

> docker image load \< 镜像.tar

<a><b> 项目打包成镜像(-t 表示指定镜像仓库名称/镜像名称:镜像标签版本 .表示使用当前目录下的Dockerfile文件)

> docker build -t mall/mall-admin:1.0-SNAPSHOT  .

### 容器相关命令

<a><b> 利用tomcat镜像新建并启动容器

- \-d：后台运行，格式：`-d 容器名:容器tag(版本)`
- \--name：指定运行后容器的名字为tomcat,之后可以通过名字来操作容器，格式：`--name 容器NAME`
- \-p：指定端口映射，格式为：`-p 宿主机端口:容器端口`，将容器端口映射到主机的端口
- 后面的:8.0.52为tomcat版本号(tag)，不指定则默认latest最新版本
- 下面的命令指：在后台启动运行一个名为“tomcat-name”的容器，访问localhost:1234可访问tomcat首页
  > docker run --name tomcat-name -d -p 1234:8080 tomcat:8.0.52  #这里版本为8.0.52，8.5版本的貌似没有启动首页，所以不能访问tomcat首页
      * 如果要挂载一个指定目录到宿主机上，则使用`-v` ，格式：`-v 宿主机目录:容器目录(也叫数据卷)`
      * 如果要连接到另一个容器相依赖，则使用`--link`(比如某个项目启动依赖mysql数据库)，格式：`--link 容器NAME:自定义容器别名`

<a><b> 一键启动所有容器(无依赖项的容器)

- 因有的容器启动需要依赖其它的容器服务(有先后顺序)，所以会启动失败。
  - 解决办法：
    > docker start 失败容器NAME/ID

<a><b> 一键关闭所有容器

> docker stop $(docker ps -a | awk '{ print $1}' | tail -n +2)

<a><b> 查看所有容器

> docker ps -a

<a><b> 进入正在运行的容器内部bash(执行下面命令后，再执行ls -l可看容器内的文件和文件夹)

>  docker exec -it 容器NAME/ID /bin/bash #相当于进入对应的安装根目录

<a><b> 停止容器

> docker stop 容器NAME/ID

<a><b> 停止所有正在运行的容器

> docker stop $(docker ps -q)

<a><b> 删除容器(只能删除停止的容器，正在运行的容器会删除失败)

> docker rm 容器NAME/ID

<a><b> 删除所有容器

> docker rm $(docker ps -aq)

<a><b> 查看容器运行日志

> docker logs 容器NAME/ID 

<a><b> 动态查看容器运行日志

> docker logs 容器NAME/ID -f

<a><b> 查看容器IP地址

```perl
docker inspect --format '{{ .NetworkSettings.IPAddress }}' 容器NAME/ID
```

<a><b> 监控容器资源消耗

> docker stats 容器NAME/ID 

<a><b> 监控所有容器资源消耗(cpu、内存、io、网络等)

> docker stats 容器NAME/ID -a

<a><b> 查看docker占用磁盘大小(根据镜像、容器)

> docker system df

<a><b> 具体查看每个镜像和容器占用磁盘大小

> docker system df -v

# Docker展现了什么效果

- 当把SpringBoot项目所有要的环境(redis、es、mongodb等，配置好相关的ip、端口、es账号等)和数据(.sql文件导入到mysql镜像中)准备好，就可以用maven插件(pom.xml中配置好Docker的ip和端口)打包成Docker镜像文件，在Docker那边就可以把image镜像文件运行为容器，这一系列操作下来，项目第一次升级成功，客户就可以远程访问了。
- 可以把自己配置好的镜像打包成tar包，然后发给同事或者测试运维人员，他们再把tar解压到docker中去就能使用了。免去麻烦的配置过程。
- 下载Pull镜像方便，容器启动也快速等等。
