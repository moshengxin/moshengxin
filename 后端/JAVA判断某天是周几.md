## java判断周六周日，周一到周五

!> **这里利用到了Calendar类**

```java
DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
String Date = "2020-08-03"; //定义初始是周一
Date testdate = sdf.parse(Date);
Calendar cal = Calendar.getInstance();
cal.setTime(testdate);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周一=========");
}
//日期加一天
cal.add(Calendar.DATE, 1);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周二=========");
}
cal.add(Calendar.DATE, 1);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周三=========");
}
cal.add(Calendar.DATE, 1);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周四=========");
}
cal.add(Calendar.DATE, 1);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周五=========");
}
cal.add(Calendar.DATE, 1);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周六=========");
}
cal.add(Calendar.DATE, 1);
if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
{
    System.out.println(sdf.format(cal.getTime()) + "=========" + "是周日=========");
}
```

> 结果打印：

    2020-08-03=========是周一=========
    2020-08-04=========是周二=========
    2020-08-05=========是周三=========
    2020-08-06=========是周四=========
    2020-08-07=========是周五=========
    2020-08-08=========是周六=========
    2020-08-09=========是周日=========

    记录代码，记录工作，记录自己。
