# Maven父子工程包管理问题

1. **pom.xml中定义模块的相关信息，一般生成项目的时候就已经有了。**

```xml
<groupId>com.example</groupId>
<artifactId>demo</artifactId>
<version>0.0.1-SNAPSHOT</version>
<!--打包类型-->
<packaging>pom</packaging>
<name>demo</name>
<!--项目/模块描述-->
<description>这是一个demo项目</description>
```

2. **父工程**: `没有src目录，只有一个pom.xml文件，并且pom文件中要定义打包方式为pom，即<packaging>pom</packaging>` 

3. **父工程中，在<dependencyManagement> 标签中统一定义依赖的版本，在<dependencyManagement>  中定义的一般是所有模块公用的依赖，如果某个依赖只有在该模块使用，其它子模块没有用到，也可以自行在自己的pom文件中进行定义。**

4. **<dependencyManagement>  的下层结构如下，<version>标签的版本号可在<properties>中定义，并使用${name}获取**

```xml
<dependencyManagement> 
    <dependencies>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>${springfox-swagger2-version}</version>
        </dependency>
    </dependencies>
</dependencyManagement>  
```

5. **<dependencyManagement>里边的<version>可以在<properties></properties>中定义，如下所示**

```xml
<properties>
    <springfox-swagger2-version>1.8</springfox-swagger2-version>
</properties>
```

6. **<dependencyManagement>中定义的依赖，并不会下载到父工程中去，他只是提供一个统一的版本给子模块去调用，方便管理，所以叫Management。**

7. **如果在<dependencyManagement></dependencyManagement>外边定义如下结构，则会在所有的子模块中继承定义的所有依赖**

```xml
<dependencyManagement>
   <!--省略。。。-->
</dependencyManagement>
<!--子模块会继承下面所有的依赖-->
<dependencies>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <scope>provided</scope>
    </dependency>
</dependencies>
```

8. **父模块的pom中，使用如下结构定义他的所有子模块**

```xml
<modules>
 <module>子模块名</module>
</modules>
```

9. **子模块中的pom文件要使用如下定义他的父模块相关信息，对应第1点。**

```xml
<parent>
    <groupId>com.example</groupId>
    <artifactId>demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</parent>
```
