# 部署mall项目爬坑日记

[原作者项目](https://github.com/macrozheng/mall)
_环境：ubuntu20_ 

## docker容器相关的坑

### 发现容器打印的日志和当前时间有出入，相差了8小时，后面得知容器和宿主机的时区不同导致

!> **解决办法有如下几种：**
> 1.在刚开始创建容器的时候，docker run后面加上`-v /etc/localtime:/etc/localtime:ro`
 
> 2.已创建的容器运行(没有/etc/localtime)，`docker cp /etc/localtime 容器NAME:/etc/`
 
> 3.进入已创建的容器内(有/etc/localtime)，先删除容器内的localtime文件`rm /etc/localtime` 再关联运行`ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime`

### 发现宿主机的端口和容器端口相同冲突

- **问题：因为宿主机之前有安装mysql和redis，并设置了开机自启，所以在启动mysql和redis容器时，会报错`Error starting userland proxy: listen tcp4 0.0.0.0:端口号: bind: address already in use`**

!> **解决办法有两种：**
  
> 1.`systemctl stop 服务名`停止相关服务，再启动容器
  
> 2.docker run创建容器的时候，更换不同宿主机端口

### SpringBoot中mysql和redis的IP指向问题

- **问题：看到yml配置中的mysql和redis的IP地址分别是`db`和`redis`两个变量，全局搜索也找不到指定的是哪个IP**
!> **解决办法：**

> 是因为mall项目用maven打包成镜像后，docker run 启动的时候要`--link`关联mysql容器的redis容器，具体看下图可知指向的是linux服务器的IP :joy:

![配置文件](https://img-blog.csdnimg.cn/img_convert/7e05ffa716a8e6600c3dbda6a58a4169.png)

## 数据库相关的坑

### 库中数据正常，但前端页面文字显示乱码

!> **思路如下：**

> 1.status 命令查看数据库mall编码，发现编码是latinl并不是utf8，于是用`set xxx=utf8`逐个改成下面格式，并重启mysql服务，重启mysql容器重试后，再次查看数据库编码还原成原来的latinal编码了，治标不治本。前端还是乱码，失败。

| character_set_client     | utf8                       |
| :----------------------- | :------------------------- |
| character_set_client     | utf8                       |
| character_set_connection | utf8                       |
| character_set_database   | utf8                       |
| character_set_filesystem | binary                     |
| character_set_results    | utf8                       |
| character_set_server     | utf8                       |
| character_set_system     | utf8                       |
| character_sets_dir       | /usr/share/mysql/charsets/ |

> 2.添加mysql配置文件my.cnf，并指定下面内容编码，重启后查看mysql编码已成功改为utf8，但是前端还是乱码。

```java
      [client]
      default-character-set=utf8
      [mysqld]
      character-set-server=utf8
      collation-server=utf8_general_ci
```

!> **最终解决方法：**

> 1.删除mall数据库，重新创建mall数据库，创建时候使用命令`create  database mall character  set utf8`指定编码,并导入mall.sql数据。

> 2.停止并删除相关容器，删除相关镜像，再重新用idea生成镜像，重新创建容器。

!> **乱码原因：<u>创建数据库时没有指定utf8编码**

## 防火墙相关坑

### 没有开放mall项目相关端口

!> **没有开放mall项目相关端口，项目部署成功却不能访问**

> 解决办法：`firewall-cmd --zone=public --add-port=端口号/tcp --permanent`
