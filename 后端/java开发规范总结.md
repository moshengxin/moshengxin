
# Java开发规范总结
## 一些命名和定义规范
> 类名使用 UpperCamelCase 风格(单词首字母大写)，必须遵从驼峰形式，但以下情形例外：DO / BO / DTO / VO / AO

> 方法名、参数名、成员变量、局部变量都统一使用 lowerCamelCase 风格(第一个单词小写，后面的都大写)

> 常量命名全部大写，单词间用下划线隔开，力求语义表达完整清楚，不要嫌名字长。

> 抽象类User命名AbstractUser，异常类UserException，测试类UserTest

> 实体类Boolean类型的属性命名都不要加 is，例如Boolean isDeleted不推荐

> 包名统一使用小写，com.公司名.项目名.模块名....或者cn.公司名.项目名.模块名....

>  Long 初始赋值时，必须使用大写的 L，不能是小写的 l，小写容易跟数字 1 混淆，造成误解。正例：Long a=10000L

## 代码格式相关
```java
// 例1
public static void main(String[] args) { 
    // 缩进 4 个空格 
    String say = "hello";
    // 运算符的左右必须有一个空格 
    int flag = 0; 
    // 关键词 if 与括号之间必须有一个空格，括号内的 f 与左括号，0 与右括号不需要空格 
    if (flag == 0) { 
       System.out.println(say); 
    }
    // 左大括号前加空格且不换行；左大括号后换行 
    if (flag == 1) { 
       System.out.println("world"); 
        // 右大括号前换行，右大括号后有 else，不用换行 
    } else { 
       System.out.println("ok");
       // 在右大括号后直接结束，则必须换行 
    } 
}  
```

> Object 的 equals 方法容易抛空指针异常，应使用常量或确定有值的对象来调用 equals。 

```java
// 正例： 
  "test".equals(object); 
// 反例： 
  object.equals("test"); 
// 说明：推荐使用 java.util.Objects#equals （JDK7 引入的工具类）
```

> 单行字符数限制不超过 120 个，超出需要换行，第二行相对第一行缩进 4 个空格，从第三行开始，不再继续缩进

> 方法参数在定义和传入时，多个参数逗号后边必须加空格。 

```java
// 正例：下例中实参的"a",后边必须要有一个空格。 
    method("a", "b", "c");
```

> 类的静态变量或静态方法直接使用类名.来访问

> 如果不允许外部直接通过 new 来创建对象，那么构造方法必须是 private。 

> 工具类不允许有 public 或 default <font color=#FF0000 >构造方法</font> 

> 工具类的方法常多用static修饰，使用的时候直接类名.方法（）

> 工具类常量定义和普通类常量定义一样，都用public static final 修饰

> 在一个 switch 块内，每个 case 要么通过 break/return 等来终止，要么注释说明程 序将继续执行到哪一个 case 为止；在一个 switch 块内，都必须包含一个 default 语句并且 放在最后，即使它什么代码也没有

```java
switch (expression) {
    case value1:
        System.out.println("do something");
        break;
    case value2:
        System.out.println("do something");
        break;
    default:
        System.out.println("do something");
}
```

> 在 if/else/for/while/do 语句中必须使用大括号。即使只有一行代码，<font color=#ff0000>避免使用 单行的形式</font>：if (condition) statements;

> 表达异常的分支时，少用 if-else 方式，这种方式可以改写成：

```java
if (condition) {
    ...
    return obj;
} 
// 接着写 else 的业务逻辑代码;
```

!> 如果非得使用 if()...else if()...else...方式表达逻辑，【强制】避免后续代码维 护困难，请勿超过 3 层。 
正例：逻辑上超过 3 层的 if-else 代码可以使用卫语句，或者状态模式来实现。卫语句示例 如下：

```java
public void today() {
    if (isBusy()) {
        System.out.println(“change time.”);
        return;
    }
    if (isFree()) {
        System.out.println(“go to travel.”);
        return;
    }
    System.out.println(“stay at home to learn Alibaba Java Coding Guideline.”);
    return;
}
```

> 如果if的判断条件表达式很长，应该将复杂逻辑判断的结果赋值给一个有意义的布尔变量名，以提高可读性

```java
final boolean existed = (file.open(fileName, "w") != null) && (...) || (...);
if (existed) {
    ...
}
```

> 不建议在循环中定义对象、变量、 获取数据库连接，以及进行不必要的 try-catch 操作，条件允许下尽可能在循环体外进行，不然会影响代码性能

## 注释相关

> 类、类属性、类方法的注释必须使用 Javadoc 规范，使用/**内容*/格式，不得使用 //xxx 方式。因为Javadoc格式鼠标移动会提示。//xxx格式却不能

> 所有的枚举类型字段必须要有注释，说明每个数据项的用途。

> 项目中注释掉的代码。说明：代码被注释掉有两种可能性：
  1）后续会恢复此段代码逻辑。
  2）如果永久不用。则删除（不用怕，代码仓库保存了历史代码）。

## 其它
> 正则表达式不要在方法体内定义

# MySQL相关

> 表达是与否概念的字段，必须使用 is_xxx 的方式命名，数据类型是 unsigned tinyint。
  * 例子：表达逻辑删除的字段名 is_deleted，1 表示删除，0 表示未删除。

> 表名、字段名必须使用小写字母或数字。例子：getter_admin，task_config，level3_name

> 主键索引名为 pk_字段名；唯一索引名为 uk_字段名；普通索引名则为 idx_字段名

> 小数类型为 decimal，禁止使用 float 和 double（精度会丢失）

> char 类型的存放固定长度的数据  如：身份证号(18） 手机号电话号(11)  性别(1)等等，如果存储的数据的大小比定义时的小，会自动补全。存储固定的长度

> varchar类型长度可变。

> in 操作能避免则避免，若实在避免不了，需要仔细评估 in 后边的集合元素数量，控 制在 1000 个之内。

> TRUNCATE TABLE 比 DELETE 速度快，TRUNCATE 无事务且不触发 trigger，有可能造成事故，故不建议在开发代码中使用此语句。TRUNCATE TABLE 在功能上与不带 WHERE 子句的 DELETE 语句相同。

> 在表查询中，一律不要使用 > 作为查询的字段列表，需要哪些字段必须明确写明。

***
摘录自某阿里巴巴开发手册，哪个版本的忘记了:pig:。