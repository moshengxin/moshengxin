# Java 日期往后加30天 非工作日
> 一开始我是用Long时间戳毫秒来计算，不知怎么地，我得出的时间没有加，反而变少了，所以后面就用了Calendar类进行操作，得出想要的结果 。

> 下面是打断点得出的时间错误的图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200528151224282.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNjkyMDIx,size_16,color_FFFFFF,t_70)

> 修改后的代码。只列出主要部分

```java
//能拿到推后30天时间的代码
 //获取系统当前时间
     Calendar currentdate = Calendar.getInstance();
     //在这里进行加30天（ps:周六周日也算在里边了）
     currentdate.add(Calendar.DATE, 30);
     //得到最后的时间
     Date finalday = currentdate.getTime(); 
```

!> 结尾：第一次分享，好怕怕。
