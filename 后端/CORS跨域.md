# URL的几个组成部分

> url组成一般主要分为`协议`、`域名`、`端口`、`请求页面路径`、`参数`这几个部分

# CORS是什么

> CORS全称Cross-Origin Resource Sharing，意为跨域资源共享。

# CORS有什么用(应用场景)

**跨域的作用是为了应用的数据安全考虑，不过又在某些时候，不得不开启跨域来达到我们的目的。**

> 当一个资源去访问另一个不同域名或者同域名不同端口的资源时，就会发出跨域请求。如果此时另一个资源不允许其进行跨域资源访问，那么访问的那个资源就会遇到跨域问题。

- **举例子：**
  - **1.在平时开发前后端分离项目中，前端vue项目的端口是8088，而后端项目是8080端口，虽然他们都是localhost主机下，但是端口不一样，前端请求后端接口时，如果后端接口没有设置允许跨域访问，就会访问失败**
  
  - **2.由于AJAX请求只能同源(协议、域名、端口三者都相同)使用的限制，所以前端使用了ajax请求而不设置允许跨域的，都会访问失败。**
  
    ![后端没开跨域.png](https://z3.ax1x.com/2021/04/12/cBUMwV.png)

# CORS怎么用(解决办法)

- ### 法1.配置类
  - **创建CorsConfiguration实例，显式指定哪些请求可跨域**

```java
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @description: 开启跨域
 * @author: Mosey
 * @time: 2021/4/10 15:38
 */
@Configuration
public class CorsConfig {


    private CorsConfiguration buildConfig() {
        //默认情况下，新创建的CorsConfiguration不允许任何跨域请求，并且必须显式配置以指示应允许的内容
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        // 显式指定哪些请求允许跨域
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");

        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        //为指定的路径模式注册CorsConfiguration
        source.registerCorsConfiguration("/**", buildConfig());

        return new CorsFilter(source);
    }


}
```

- ### 法2.@CrossOrigin注解
> 查看@CrossOrigin源码得知， 该注解可用在类，接口，枚举和方法上 ，也可以类上和方法上结合一起使用。
  
> `@CrossOrigin`的属性有设置默认值，如果想用更加细的颗粒度来控制跨域，则看自己的业务需求，这里不多介绍。

- ### 法3.node.js配置文件开启
  - **如果前端项目为NodeJs构建的项目，可在配置文件index.js中的代理中开启跨域`changeOriginL: true`(这样前端的localhost:8083请求传到后台会变成localhost:8080,达到同源)。**
  ![cDewSU.png](https://z3.ax1x.com/2021/04/12/cDewSU.png)

- ### 法4.jsonp（不建议)
  - **将cors请求伪装成jsonp请求**
    >  dataType:"jsonp",
    >  jsonp:"jsonpcallback"

- ### 法5.nginx配置
  具体请[参与](https://segmentfault.com/a/1190000011145364)

- ### 法6.springsecurity配置文件
  - 如下图![cDKZjS.png](https://z3.ax1x.com/2021/04/12/cDKZjS.png)

- ### 法7.H5新特性

- **`window.postMessage(message,targetOrigin)`只能在两个客户端(可以不同源)的页面之间进行数据交换，<a>不能和服务端进行数据交换</a>**
