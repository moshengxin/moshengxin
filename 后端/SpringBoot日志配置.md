# SpringBoot日志配置

> `SpringBoot` 默认集成了 `logback` 日志框架，不用再添加 `logback` 依赖，`logback是基于Slf4j实现`

> 日志级别由低到高：**TRACE** < **DEBUG** < **INFO** < **WARN** < **ERROR** < **FATAL**

> 如果日志级别设置成 `ERROR` 则可以输出 `ERROR` 左边的所有级别，如果设置为 `INFO` ，则只有 `TRACE`和 `DEGUG`和 `INFO` 本身可以输出

> SpringBoot 默认 `INFO` 级别

## logback打印日志的使用方法
### 方法1（推荐）
> 使用到 `lombok` 包下的 `@Slf4j` 注解，并作用在类上，然后在类中的所有方法体中使用 `log.info("request params is {}",params.toString())` 打印即可

### 方法2（不推荐）
```java
private final Logger log= LoggerFactory.getLogger(对应类.class);
log.info("输出")
```
![输出控制台](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/2022下半年/输出控制台.6h43pzovd240.webp)
## logback配置文件相关
> 配置文件名：`logback-spring.xml(推荐)`、`logback.xml(其次)`

> 配置文件存在位置：`src/resources`文件夹下

### 配置文件主要内容和一些标签解释
#### 父标签configuration
>  `<configuration debug="false" scan="false" scanPeriod="30 seconds"></configuration>`
   - scan=true时，当配置被修改，重新加载时获取不到logFileName对应的值
   
   - debug：默认为false，设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。
   
   - scan：配置文件如果发生改变，将会重新加载，默认值为true。
   
   - scanPeriod：检测配置文件是否有修改的时间间隔，如果没有给出时间单位，默认单位时毫秒。
   
   - 当scan为true时，这个属性生效，默认时间间隔为1min，单位有milliseconds, seconds, minutes or hours。

![配置详情1](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/2022下半年/配置详情1.17ffedzv7gyo.webp)

#### 追加标签appender
>  `<appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender"></appender>`，其中class属性中包含 `ConsoleAppender` 的是输出到控制台，`RollingFileAppender` 是输出到文件

![配置2](https://cdn.jsdelivr.net/gh/moshengxin/Blog-Picture@main/2022下半年/配置2.759n05mmv480.webp)

#### 一些日志转换符说明
> `%d [%t]-[%c:%line]-%p %m%n` 解读
   
   - %d：date代表日志输出时间
   
   - %t: thread代表日志对应线程名称
   
   - %c: .class代表日志对应的.class文件
   
   - %line: 代表日志输出位置的行号
   
   - %p: p/le/level代表日志输出级别
   
   - %m: method代表日志输出时对应的方法名
   
   - %n: 换行

#### logger标签
> `标签<logger name="org.springframework.web" level="INFO"/>`
   
   - 代表org.springframework.web包下的类的日志级别为INFO

