# 两种整合方法

!> **一种是spring-boot-starter-data-redis**

!> **另一种是jedis**

## 方法一

> 1、添加依赖

```xml
<!--redis依赖配置--> 
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

> 2、SpringBoot配置文件定义redis相关

```yml
spring
  redis:
    host: localhost # Redis主机地址
    database: 0 # Redis数据库下标（从0开始，默认第一个库）
    port: 6379 # Redis端口
    password: # Redis密码（默认为空）
    jedis:
      pool:
        max-active: 8 # 连接池最大连接数（使用负值表示没有限制）
        max-wait: -1ms # 连接池最大阻塞等待时间（使用负值表示没有限制）
        max-idle: 8 # 连接池中的最大空闲连接
        min-idle: 0 # 连接池中的最小空闲连接
        timeout: 3000ms # 连接超时时间（毫秒）
# 自定义redis的key的前缀和到期时间
redis:
  key:
    prefix:
      authCode: "portal:authCode:"
    expire:
      authCode: 120 # 验证码超期时间
```

> 3、定义一个redis的常用操作接口

```java
public interface RedisService {
    /**
 * 存储数据
  */
  void set(String key, String value);

    /**
 * 获取数据
  */
  String get(String key);

    /**
 * 设置超期时间
  */
  boolean expire(String key, long expire);

    /**
 * 删除数据
  */
  void remove(String key);

    /**
 * 自增操作
  * @param delta 自增步长
  */
  Long increment(String key, long delta);

}
```

> 4、创建RedisService接口的实现类RedisServiceImpl，并在实现类中导入第一步依赖中的StringRedisTemplate类

```java
@Service
public class RedisServiceImpl implements RedisService {
    @Autowired
  private StringRedisTemplate stringRedisTemplate;

    @Override
  public void set(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    @Override
  public String get(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    @Override
  public boolean expire(String key, long expire) {
        return stringRedisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }

    @Override
  public void remove(String key) {
        stringRedisTemplate.delete(key);
    }

    @Override
  public Long increment(String key, long delta) {
        return stringRedisTemplate.opsForValue().increment(key,delta);
    }
}
```

!> RedisTemplate的以字符串为中心的扩展。 由于大多数针对Redis的操作都是基于String的，因此此类提供了一个专用类，该类可最大程度地减少其通用template配置，尤其是在序列化程序方面。

> 5、使用方法

- **1.在要使用的类中，注入接口**

```java
 @Autowired 
 private RedisService redisService;
```

- **2.get set expire使用**

```java
//取出配置文件中自定义的key的前缀和到期时间
@Value("${redis.key.prefix.authCode}")
private String REDIS_KEY_PREFIX_AUTH_CODE;

@Value("${redis.key.expire.authCode}")
private Long AUTH_CODE_EXPIRE_SECONDS;


 //设置值
 redisService.set(REDIS_KEY_PREFIX_AUTH_CODE+"任一字符串",value)  
 //获取值  
 redisService.get(key)  
 //设置到期时间expirationdate
 redisService.expire(key,AUTH_CODE_EXPIRE_SECONDS) 
 
```

## 方法二

!> 配置文件和上面的一样，只是依赖和工具类不一样。个人觉得这种方法好点。这个能操作除了String类型外，还可以操作其它类型的数据。

> 1.添加jedis依赖

```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>3.2.0</version>
</dependency>
```

> 2.JedisUtil工具类编写

```java
@Component 
public class JedisUtil {

    @Value("${spring.redis.host}")
    private String redis_host;

    @Value("${spring.redis.port}")
    private Integer redis_port;

    @Value("${spring.redis.jedis.pool.max-idle}")
    private Integer max_idle;

    @Value("${spring.redis.jedis.pool.min-idle}")
    private Integer min_idle;

    //获取redis操作对象
  public Jedis getJedis() {

        String host = redis_host;
        int port = redis_port;
        int maxTotal = max_idle;
        int maxIdle = min_idle;
        //设置配置信息
  JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMaxTotal(maxTotal);

        //初始化
  JedisPool jedisPool = new JedisPool(jedisPoolConfig, host, port);
        Jedis jedis=jedisPool.getResource();
        return jedis;
    }

}
```

> 3.使用方法

```java
//注入工具类
@Autowired 
private JedisUtil jedisUtil;

省略...

//获取jedis对象，操作String类型，其它类型的省略。
Jedis jedis = jedisUtil.getJedis();
jedis.set("key","value");
```
