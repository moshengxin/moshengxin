# SpringBoot整合Swagger-UI接口文档

## Swagger-UI好处

!>**Swagger-UI   一个好看又好用的项目接口文档，可以动态地根据注解生成在线API文档。也就是说和你代码的Controller层的接口方法对应，支持在线接口测试，不依赖第三方工具**

## Swagger-UI常用注解介绍

> @Api：用于修饰Controller类，生成Controller相关文档信息

> @ApiOperation：用于修饰Controller类中的方法，生成接口方法相关文档信息

> @ApiParam：用于修饰接口中的参数，生成接口参数相关文档信息

> @ApiModelProperty：用于修饰实体类的属性，当实体类是请求参数或返回结果时，直接生成相关文档信息

## 整合步骤

### 1、maven中添加依赖

```java
    <!--Swagger-UI API文档生产工具-->
    <dependency>
      <groupId>io.springfox</groupId>
      <artifactId>springfox-swagger2</artifactId>
      <version>2.7.0</version>
    </dependency>
    <dependency>
      <groupId>io.springfox</groupId>
      <artifactId>springfox-swagger-ui</artifactId>
      <version>2.7.0</version>
    </dependency>
```

### 2、项目中添加Swagger-UI配置类

这里参考github上mall项目的配置类

```java
package com.macro.mall.tiny.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2API文档的配置
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket createRestApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //为当前包下controller生成API文档
                .apis(RequestHandlerSelectors.basePackage("com.macro.mall.tiny.controller"))
                //为有@Api注解的Controller生成API文档
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                //为有@ApiOperation注解的方法生成API文档
               .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SwaggerUI文档")   
                .description("某某某商城项目/系统后台所有接口文档")
                .contact("macro")
                .version("版本号1.0")
                .build();
    }
}
```

### 3、配合注解使用

**在你要生成接口文档的Controller层的Class类上面添加@Api(tags = "PmsBrandController", description = "商品品牌管理")方法上面添加@ApiOperation("获取所有品牌列表")注解**

[![Bwdkq0.png](https://s1.ax1x.com/2020/11/01/Bwdkq0.png)](https://imgchr.com/i/Bwdkq0)

### 4、查看效果

**访问接口文档地址接口地址：<http://localhost:8086/swagger-ui.html> 查看效果**

> 注意换成自己项目的端口号（application.yml中查看或者设置springboot项目端口），我这里使用的是8086端

[![Bwd2WQ.png](https://s1.ax1x.com/2020/11/01/Bwd2WQ.png)](https://imgchr.com/i/Bwd2WQ)
参考：<https://github.com/macrozheng/mall>
