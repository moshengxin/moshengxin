* **前端**
  - [**Js常用**](/前端/JavaScript常用操作.md)

  - [**Array常用方法**](/前端/JavaScript中Array的一些常用方法和操作.md)

  - [**父子组件通信**](/前端/子组件中调用父组件数据.md)

  - [**Js排序**](/前端/前端js排序.md)

  - [**常用样式**](/前端/前端一些样式记录.md)

  - [**返回顶部按钮**](/前端/自己DIY一个返回顶部.md)

* **后端**
  - [**日期往后加30天**](/后端/日期往后加30天(非工作日).md)
  - [**代码规范**](/后端/java开发规范总结.md)
  - [**判断某天是周几**](/后端/JAVA判断某天是周几.md)
  - [**Jvm整理**](/后端/jvm整理.md)
  - [**List去重**](/后端/list去重方法.md)
  - [**高并发下保证幂等性**](/后端/高并发下如何保证幂等性.md)
  - [**设计模式**](/后端/面试官老问我设计模式.md)
  - [**docker容器**](/后端/docker容器.md)
  - [**SpringBoot整合ElastitcSearch中文分词**](/后端/SpringBoot整合ElastitcSearch中文分词.md)
  - [**Mongodb**](/后端/Mongodb记录用户浏览商品记录.md)
  - [**SpringBoot整合Redis**](/后端/SpringBoot整合redis.md)
  - [**SpringBoot整合RMQ实现延迟消息**](/后端/SpringBoot整合RMQ实现延迟消息.md)
  - [**SpringBoot整合Swagger-UI接口文档**](/后端/SpringBoot整合Swagger-UI接口文档.md)
  - [**Spring实现Aop切面编程**](/后端/spring实现切面编程.md)
  - [**ActiveMq**](/后端/ActiveMq.md)
  - [**CORS跨域解决方案**](/后端/CORS跨域.md)
  - [**项目部署总结**](/后端/部署mall项目遇到的问题和解决方案.md)
  - [**Maven父子工程包管理**](/后端/Maven父子工程包管理问题.md)
  - [**SpringBoot日志配置**](/后端/SpringBoot日志配置.md)
  - [**SpringMvc拦截器HandlerInterceptor**](/后端/SpringMvc拦截器HandlerInterceptor.md)
* **服务器**
  - [**Linux命令和软件**](/服务器/linux.md)
  - [**Nginx总结**](/服务器/nginx服务器.md)
* **数据库**
  - [**查看Sql执行效率**](/数据库/Explain查询Sql效率.md)
  - [**Postgres备份和还原**](/数据库/postgres数据库备份和还原命令-亲测有效.md)
  - [**多数据源配置**](/数据库/多数据源配置.md)
  - [**Redis**](/数据库/redis相关总结.md)
* [**相册**](/其它/相册.md)
