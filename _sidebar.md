- [关于本站](/作者/link)

* **小前端**
- [flex布局](/前端/flex布局 "flex布局")
- [Js 常用](/前端/JavaScript常用操作 "js常用操作")
- [Array 常用方法](/前端/JavaScript中Array的一些常用方法和操作 "js常用方法")
- [Vue 父子组件通信](/前端/子组件中调用父组件数据 "父子组件通信")
- [Js 排序](/前端/前端js排序 "js实现排序")
- [常用样式](/前端/前端一些样式记录 "前端css常用样式")
- [返回顶部](/前端/自己DIY一个返回顶部 "返回顶部")

* **小后端**
- [日期往后加 30 天](/后端/日期往后加30天(非工作日) "java日期")
- [代码规范](/后端/java开发规范总结 "java开发规范")
- [判断某天是周几](/后端/JAVA判断某天是周几 "java判断是周几")
- [Jvm 整理](/后端/jvm整理 "jvm整理")
- [List 去重](/后端/list去重方法 "java list 去重")
- [高并发下保证幂等性](/后端/高并发下如何保证幂等性 "高并发 幂等性")
- [设计模式](/后端/面试官老问我设计模式 "java 设计模式")
- [docker 容器](/后端/docker容器 "docker 容器")
- [SpringBoot 整合 ElastitcSearch 中文分词](/后端/SpringBoot整合ElastitcSearch中文分词 "springboot elasticsearch 分词")
- [Mongo](/后端/Mongodb记录用户浏览商品记录 "Mongodb")
- [SpringBoot 整合 Redis](/后端/SpringBoot整合redis "springboot redis")
- [SpringBoot 整合 RMQ 实现延迟消息](/后端/SpringBoot整合RMQ实现延迟消息 "springboot rmq 消息延迟")
- [SpringBoot 整合 Swagger-UI 接口文档](/后端/SpringBoot整合Swagger-UI接口文档 "springboot接口文档 swagger-ui")
- [Spring 实现 Aop 切面编程](/后端/spring实现切面编程 "spring aop 切面")
- [ActiveMq](/后端/ActiveMq "activemq")
- [CORS 跨域解决方案](/后端/CORS跨域 "跨域解决方案")
- [项目部署](/后端/部署mall项目遇到的问题和解决方案 "项目部署")
- [Maven父子工程包管理](/后端/Maven父子工程包管理问题 "Maven父子工程包管理问题")
- [SpringBoot日志配置](/后端/SpringBoot日志配置 "SpringBoot日志配置")
- [SpringMvc拦截器HandlerInterceptor](/后端/SpringMvc拦截器HandlerInterceptor "SpringMvc拦截器HandlerInterceptor")

* **小程序/公众号**
- [公众号推送消息](/小程序or公众号/微信公众号H5消息推送功能 "微信开发")

* **服务器相关**
- [Linux 命令和软件](/服务器/linux "服务器开发")
- [Nginx 总结](/服务器/nginx服务器 "nginx")

* **数据库**
- [查看 Sql 执行效率](/数据库/Explain查询Sql效率 "explain sql效率")
- [Postgres 备份和还原](/数据库/postgres数据库备份和还原命令-亲测有效 "postgres备份和还原")
- [多数据源配置](/数据库/多数据源配置 "多数据源配置")
- [Redis](/数据库/redis相关总结 "redis基础")

* **其它**
- [表情管理](/其它/摊牌了！想做表情管理大师？"markdown")
- [相册](/其它/相册)
- [plan](/其它/plan)

