---
title: List去重总结
date: 2021-01-17 17:57:15
tags: java
categories: List 集合
---
# List去重总结 

## List和Set区别
Java中的集合分三大类，分别是Set（集），List（列表）以及Map（映射）
1、List和Set都是接口继承于Collection接口。

2、List有序可重复的。而Set无序不可重复的

3、List接口的三个实现类：LinkedList，ArrayList，Vector ，Set接口的两个实现类：HashSet，LinkedHashSet

4、List适合经常追加数据，插入，删除数据。但随机取数效率比较低。

5、Set适合经常地随机储存，插入，删除。但是在遍历时效率比较低。 
## 去重方法
* 1、
```java
ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1,2,2,3,3,3,4,4));
HashSet<Integer> set = new HashSet<Integer>(list1.size());  
List<Integer> newlist= new ArrayList<Integer>(list1.size());  
for (Integer it : list1) {
    if (set.add(it)) {   //运用Set无序不可重复特点来判断
        newlist.add(it);  //把不重复的数据添加到新的newlist
    }
}
list1.clear();   //循环后把原list1清空
list1.addAll(newlist);  //再把新的newlist全部加到list1中去，得到去重后的list1
System.out.println(list1);  //[1, 2, 3, 4]
```
* 2、list转set自动去重
```java
//list2转set的时候去重,set再转newlist2
LinkedList<Integer> list2= new LinkedList<>(Arrays.asList(1,2,2,3,3,3,4,4));

LinkedHashSet<Integer> lhset= new LinkedHashSet<>(list2);

LinkedList<Integer> newlist2= new LinkedList<>(lhset);

System.out.println(newlist2);//[1, 2, 3, 4]
```
* 3、遍历list3，用contains判断有重复的就不存放到newlist3中去。循环结束后把list3清空，再把newlist3全部放进去，得到去重后的list3
```java
// 
LinkedList<Integer> list3= new LinkedList<>(Arrays.asList(1,2,2,3,3,3,4,4));
LinkedList<Integer> newlist3=new LinkedList<>();
for (int i = 0; i < list3.size(); i++) {
    if(!newlist3.contains(list3.get(i))){
        newlist3.add(list3.get(i));
    }
}
list3.clear();
list3.addAll(newlist3);
System.out.println(list3); //[1, 2, 3, 4]
```
* 4、用Stream流的distinct方法去重
```java
 LinkedList<Integer> list4= new LinkedList<>(Arrays.asList(1,2,2,3,3,3,4,4));
 Stream<Integer> liststream= list4.stream().distinct();

// distinct.forEach(res-> System.out.println(res));  //输出每一项    
//重复后的liststream转成newlist4
List<Integer> newlist4= liststream.collect(Collectors.toList());  
 System.out.println(newlist4);//[1, 2, 3, 4]
```
* 5、如果List中存放的是Entity类对象，要根据某个字段去重，可考虑TreeSet去重
```java
//实体类
public class Student {
    private String name;
    private int age;
    private double score;
    ......       //省略get、set、构造等一系列方法
    }
    
List<Student> students = Arrays.asList(
        new Student("小明", 20, 91),
        new Student("小明", 20, 90), 
        new Student("小黑", 19, 60), 
        new Student("小白", 22, 89)
        );
//定义一个Student类的TreeSet,根据Name字段比较去重的比较器
TreeSet<Student> treeSet = new TreeSet<(Comparator.comparing(Student::getName)); 
//Students实体类对象students添加到TreeSet中去进行比较
treeSet.addAll(students);
//TreeSet再转换成List
List<Student> resList= treeSet.stream().collect(Collectors.toList());
System.out.println(resList);
```

### 就着Stream,顺便记录一下排序
```java
//实体类
public class Student {
    private String name;
    private int age;
    private double score;
    ......       //省略get、set、构造等一系列方法
    }
    
List<Student> students = Arrays.asList(
        new Student("小明", 20, 91),
        new Student("小明", 20, 90), 
        new Student("小黑", 19, 60), 
        new Student("小白", 22, 89)
        );
 //students对象转流后进行sorted排序，根据Age字段排序，limit取前两条。
Stream<Student> sortedStream = students.stream().sorted((item2, item1) -> item1.getAge() - item2.getAge()).limit(2);
List<Student> newList= sortedStream.collect(Collectors.toList());
```
